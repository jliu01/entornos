﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace LibreriaVisual
{
    class Program
    {
        static void Main(string[] args)
        {
            
            int opcion = int.Parse(Console.ReadLine());
           Console.WriteLine("Introduce una opcion:"
                    + "1- Comprobar entero"
                    + "2- Rellenar array"
                    + "3- Contar hasta el 100"
                    + "4- Sumar arrays"
                    + "5- Salir");
            switch (opcion)
            {
                case 1:
                    Console.WriteLine("Introduce cadena");
                    String cadena = Console.ReadLine();

                    if (libVS.esEntero(cadena))
                    {
                        Console.WriteLine("Es entero");
                    }
                    else
                    {
                        Console.WriteLine("No es entero");
                    };
                    break;
                case 2:

                    libVS.rellenar(int.Parse(Console.ReadLine()));
                    break;
                case 3:
                    Console.WriteLine("Introduce un entero");
                    int entero = int.Parse(Console.ReadLine());
                    libVS.contar(entero);
                    break;

                case 4:
                    int[] array1 = new int[5];
                    int[] array2 = new int[5];
                    for (int i = 0; i < array1.Length; i++)
                    {
                        Console.WriteLine("Rellena el primer array con enteros");
                        array1[i] = int.Parse(Console.ReadLine());

                    }
                    for (int i = 0; i < array2.Length; i++)
                    {
                        Console.WriteLine("Rellena el segundo array con enteros");
                        array2[i] = int.Parse(Console.ReadLine());

                    }
                    libVS.sumarArrays(array1, array2);

                    break;
                case 5:
                    break;

                default:
                    Console.WriteLine("Opcion no valida");
                    break;
            }

        }
    }
}
