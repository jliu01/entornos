package ventana1;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JToolBar;
import javax.swing.JMenuBar;
import javax.swing.JList;
import java.awt.Button;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTree;
import javax.swing.JSpinner;
import javax.swing.JTabbedPane;
import javax.swing.JInternalFrame;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JTextArea;
import javax.swing.JProgressBar;
import javax.swing.LayoutStyle.ComponentPlacement;
import java.awt.Panel;
import java.awt.Component;
import javax.swing.Box;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.JToggleButton;
import java.awt.event.ContainerAdapter;
import java.awt.event.ContainerEvent;
import java.awt.Checkbox;
import javax.swing.JSlider;
import javax.swing.JLayeredPane;
import java.awt.Label;
import javax.swing.JCheckBoxMenuItem;
import java.awt.TextArea;
import java.awt.Dimension;
import javax.swing.JEditorPane;
import javax.swing.JComboBox;
import javax.swing.JCheckBox;
import java.awt.Canvas;
import java.awt.Color;
import java.awt.Font;
import javax.swing.JSeparator;
import javax.swing.JTextPane;
import javax.swing.JDesktopPane;
import javax.swing.JLabel;

public class Ventana1 extends JFrame {

	private JPanel contentPane;
	private JPasswordField passwordField;
	private JTextField txtUsuario;
	private JTextField txtBuscador;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Ventana1 frame = new Ventana1();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Ventana1() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 685, 459);
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JButton btnNewButton = new JButton("Inicio");
		btnNewButton.addContainerListener(new ContainerAdapter() {
			@Override
			public void componentAdded(ContainerEvent arg0) {
			}
		});
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		menuBar.add(btnNewButton);
		
		JButton btnNewButton_1 = new JButton("Cat�logo");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		menuBar.add(btnNewButton_1);
		
		JButton btnNewButton_2 = new JButton("Ofertas");
		menuBar.add(btnNewButton_2);
		
		Component rigidArea = Box.createRigidArea(new Dimension(184, 20));
		menuBar.add(rigidArea);
		
		JButton btnNewButton_3 = new JButton("Mi cuenta");
		menuBar.add(btnNewButton_3);
		
		JButton btnNewButton_4 = new JButton("Ayuda");
		menuBar.add(btnNewButton_4);
		
		JButton btnNewButton_5 = new JButton("Mi carrito");
		menuBar.add(btnNewButton_5);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		
		JButton btnNewButton_6 = new JButton("Buscar");
		
		JProgressBar progressBar = new JProgressBar();
		
		passwordField = new JPasswordField();
		passwordField.setToolTipText("");
		
		txtUsuario = new JTextField();
		txtUsuario.setColumns(10);
		
		JButton btnIniciarSesin = new JButton("Iniciar sesi\u00F3n");
		
		txtBuscador = new JTextField();
		txtBuscador.setText("Buscador...");
		txtBuscador.setColumns(10);
		
		JToolBar toolBar = new JToolBar();
		
		JCheckBox chckbxNoGuardarResultads = new JCheckBox("No guardar resultados");
		
		JSeparator separator = new JSeparator();
		
		JTextPane txtpnBuildapc = new JTextPane();
		txtpnBuildapc.setFont(new Font("Monospaced", Font.BOLD, 20));
		txtpnBuildapc.setEditable(false);
		txtpnBuildapc.setText("BuildAPC");
		
		JLabel lblNewLabel = new JLabel("Usuario");
		
		JLabel lblContrasea = new JLabel("Contrase\u00F1a");
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING, false)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addGap(10)
							.addComponent(progressBar, GroupLayout.PREFERRED_SIZE, 641, GroupLayout.PREFERRED_SIZE))
						.addComponent(toolBar, GroupLayout.PREFERRED_SIZE, 651, GroupLayout.PREFERRED_SIZE)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addGap(139)
							.addComponent(txtBuscador, GroupLayout.PREFERRED_SIZE, 371, GroupLayout.PREFERRED_SIZE))
						.addGroup(gl_contentPane.createSequentialGroup()
							.addGap(282)
							.addComponent(btnNewButton_6))
						.addGroup(gl_contentPane.createSequentialGroup()
							.addGap(232)
							.addComponent(chckbxNoGuardarResultads, GroupLayout.PREFERRED_SIZE, 174, GroupLayout.PREFERRED_SIZE))
						.addGroup(gl_contentPane.createSequentialGroup()
							.addGap(10)
							.addComponent(txtUsuario, GroupLayout.PREFERRED_SIZE, 111, GroupLayout.PREFERRED_SIZE)
							.addGap(18)
							.addComponent(passwordField, GroupLayout.PREFERRED_SIZE, 117, GroupLayout.PREFERRED_SIZE)
							.addGap(10)
							.addComponent(btnIniciarSesin))
						.addGroup(Alignment.TRAILING, gl_contentPane.createSequentialGroup()
							.addGap(40)
							.addComponent(lblNewLabel, GroupLayout.PREFERRED_SIZE, 45, GroupLayout.PREFERRED_SIZE)
							.addGap(79)
							.addComponent(lblContrasea, GroupLayout.PREFERRED_SIZE, 73, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
							.addComponent(txtpnBuildapc, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
					.addContainerGap())
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGap(67)
					.addComponent(separator, GroupLayout.DEFAULT_SIZE, 460, Short.MAX_VALUE)
					.addGap(134))
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGroup(gl_contentPane.createParallelGroup(Alignment.TRAILING)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addComponent(txtpnBuildapc, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addGap(7))
						.addGroup(gl_contentPane.createSequentialGroup()
							.addComponent(lblNewLabel)
							.addPreferredGap(ComponentPlacement.RELATED))
						.addComponent(lblContrasea))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addGap(1)
							.addComponent(txtUsuario, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addGroup(gl_contentPane.createSequentialGroup()
							.addGap(1)
							.addComponent(passwordField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addComponent(btnIniciarSesin))
					.addGap(28)
					.addComponent(separator, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addGap(68)
					.addComponent(txtBuscador, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addGap(28)
					.addComponent(btnNewButton_6)
					.addGap(15)
					.addComponent(chckbxNoGuardarResultads)
					.addGap(18)
					.addComponent(progressBar, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addGap(18)
					.addComponent(toolBar, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
		);
		contentPane.setLayout(gl_contentPane);
	}
}
