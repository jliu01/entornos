﻿namespace VentanaVisual
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.notifyIcon1 = new System.Windows.Forms.NotifyIcon(this.components);
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.vScrollBar1 = new System.Windows.Forms.VScrollBar();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.inicioToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.categoríasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ordenadoresToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sobremesaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ultrabooksToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.chromebooksToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.portátilesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.gamingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.paraTrabajarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.periféricosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ratonesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tecladoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.altavocesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tarjetasGráficasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.procesadoresToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ofertasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.enPromociónToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.miCuentaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.misDatosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.misPedidosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.artículosGuardadosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mensajesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cerrarSesiónToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.miCarritoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ayudaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.linkLabel1 = new System.Windows.Forms.LinkLabel();
            this.linkLabel2 = new System.Windows.Forms.LinkLabel();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.directorySearcher1 = new System.DirectoryServices.DirectorySearcher();
            this.helpProvider1 = new System.Windows.Forms.HelpProvider();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(481, 28);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(150, 91);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // notifyIcon1
            // 
            this.notifyIcon1.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIcon1.Icon")));
            this.notifyIcon1.Text = "notifyIcon1";
            this.notifyIcon1.Visible = true;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(12, 43);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(100, 22);
            this.textBox1.TabIndex = 1;
            this.textBox1.Text = "Usuario";
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(118, 43);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(100, 22);
            this.textBox2.TabIndex = 2;
            this.textBox2.Text = "Contraseña";
            // 
            // vScrollBar1
            // 
            this.vScrollBar1.Location = new System.Drawing.Point(1011, 28);
            this.vScrollBar1.Name = "vScrollBar1";
            this.vScrollBar1.Size = new System.Drawing.Size(21, 399);
            this.vScrollBar1.TabIndex = 3;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(224, 43);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(112, 23);
            this.button1.TabIndex = 4;
            this.button1.Text = "Iniciar sesión";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(790, 44);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(112, 23);
            this.button2.TabIndex = 5;
            this.button2.Text = "Buscar";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(908, 44);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(100, 22);
            this.textBox3.TabIndex = 6;
            this.textBox3.Text = "Buscador...";
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "De mayor a menor",
            "De menor a mayor",
            "Por popularidad",
            "Los más vistos",
            "Por valoración",
            "Nuevos primero",
            "Antiguos primero"});
            this.comboBox1.Location = new System.Drawing.Point(854, 129);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(154, 24);
            this.comboBox1.TabIndex = 7;
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.inicioToolStripMenuItem,
            this.categoríasToolStripMenuItem,
            this.ofertasToolStripMenuItem,
            this.enPromociónToolStripMenuItem,
            this.miCuentaToolStripMenuItem,
            this.miCarritoToolStripMenuItem,
            this.ayudaToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1032, 28);
            this.menuStrip1.TabIndex = 8;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // inicioToolStripMenuItem
            // 
            this.inicioToolStripMenuItem.Name = "inicioToolStripMenuItem";
            this.inicioToolStripMenuItem.Size = new System.Drawing.Size(57, 24);
            this.inicioToolStripMenuItem.Text = "Inicio";
            // 
            // categoríasToolStripMenuItem
            // 
            this.categoríasToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ordenadoresToolStripMenuItem,
            this.portátilesToolStripMenuItem,
            this.periféricosToolStripMenuItem,
            this.tarjetasGráficasToolStripMenuItem,
            this.procesadoresToolStripMenuItem});
            this.categoríasToolStripMenuItem.Name = "categoríasToolStripMenuItem";
            this.categoríasToolStripMenuItem.Size = new System.Drawing.Size(92, 24);
            this.categoríasToolStripMenuItem.Text = "Categorías";
            // 
            // ordenadoresToolStripMenuItem
            // 
            this.ordenadoresToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.sobremesaToolStripMenuItem,
            this.ultrabooksToolStripMenuItem,
            this.chromebooksToolStripMenuItem});
            this.ordenadoresToolStripMenuItem.Name = "ordenadoresToolStripMenuItem";
            this.ordenadoresToolStripMenuItem.Size = new System.Drawing.Size(190, 26);
            this.ordenadoresToolStripMenuItem.Text = "Ordenadores";
            // 
            // sobremesaToolStripMenuItem
            // 
            this.sobremesaToolStripMenuItem.Name = "sobremesaToolStripMenuItem";
            this.sobremesaToolStripMenuItem.Size = new System.Drawing.Size(176, 26);
            this.sobremesaToolStripMenuItem.Text = "Sobremesa";
            // 
            // ultrabooksToolStripMenuItem
            // 
            this.ultrabooksToolStripMenuItem.Name = "ultrabooksToolStripMenuItem";
            this.ultrabooksToolStripMenuItem.Size = new System.Drawing.Size(176, 26);
            this.ultrabooksToolStripMenuItem.Text = "Ultrabooks";
            // 
            // chromebooksToolStripMenuItem
            // 
            this.chromebooksToolStripMenuItem.Name = "chromebooksToolStripMenuItem";
            this.chromebooksToolStripMenuItem.Size = new System.Drawing.Size(176, 26);
            this.chromebooksToolStripMenuItem.Text = "Chromebooks";
            // 
            // portátilesToolStripMenuItem
            // 
            this.portátilesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.gamingToolStripMenuItem,
            this.paraTrabajarToolStripMenuItem});
            this.portátilesToolStripMenuItem.Name = "portátilesToolStripMenuItem";
            this.portátilesToolStripMenuItem.Size = new System.Drawing.Size(190, 26);
            this.portátilesToolStripMenuItem.Text = "Portátiles";
            // 
            // gamingToolStripMenuItem
            // 
            this.gamingToolStripMenuItem.Name = "gamingToolStripMenuItem";
            this.gamingToolStripMenuItem.Size = new System.Drawing.Size(168, 26);
            this.gamingToolStripMenuItem.Text = "Gaming";
            // 
            // paraTrabajarToolStripMenuItem
            // 
            this.paraTrabajarToolStripMenuItem.Name = "paraTrabajarToolStripMenuItem";
            this.paraTrabajarToolStripMenuItem.Size = new System.Drawing.Size(168, 26);
            this.paraTrabajarToolStripMenuItem.Text = "Para trabajar";
            // 
            // periféricosToolStripMenuItem
            // 
            this.periféricosToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ratonesToolStripMenuItem,
            this.tecladoToolStripMenuItem,
            this.altavocesToolStripMenuItem});
            this.periféricosToolStripMenuItem.Name = "periféricosToolStripMenuItem";
            this.periféricosToolStripMenuItem.Size = new System.Drawing.Size(190, 26);
            this.periféricosToolStripMenuItem.Text = "Periféricos";
            // 
            // ratonesToolStripMenuItem
            // 
            this.ratonesToolStripMenuItem.Name = "ratonesToolStripMenuItem";
            this.ratonesToolStripMenuItem.Size = new System.Drawing.Size(148, 26);
            this.ratonesToolStripMenuItem.Text = "Ratones";
            // 
            // tecladoToolStripMenuItem
            // 
            this.tecladoToolStripMenuItem.Name = "tecladoToolStripMenuItem";
            this.tecladoToolStripMenuItem.Size = new System.Drawing.Size(148, 26);
            this.tecladoToolStripMenuItem.Text = "Teclado";
            // 
            // altavocesToolStripMenuItem
            // 
            this.altavocesToolStripMenuItem.Name = "altavocesToolStripMenuItem";
            this.altavocesToolStripMenuItem.Size = new System.Drawing.Size(148, 26);
            this.altavocesToolStripMenuItem.Text = "Altavoces";
            // 
            // tarjetasGráficasToolStripMenuItem
            // 
            this.tarjetasGráficasToolStripMenuItem.Name = "tarjetasGráficasToolStripMenuItem";
            this.tarjetasGráficasToolStripMenuItem.Size = new System.Drawing.Size(190, 26);
            this.tarjetasGráficasToolStripMenuItem.Text = "Tarjetas gráficas";
            // 
            // procesadoresToolStripMenuItem
            // 
            this.procesadoresToolStripMenuItem.Name = "procesadoresToolStripMenuItem";
            this.procesadoresToolStripMenuItem.Size = new System.Drawing.Size(190, 26);
            this.procesadoresToolStripMenuItem.Text = "Procesadores";
            // 
            // ofertasToolStripMenuItem
            // 
            this.ofertasToolStripMenuItem.Name = "ofertasToolStripMenuItem";
            this.ofertasToolStripMenuItem.Size = new System.Drawing.Size(69, 24);
            this.ofertasToolStripMenuItem.Text = "Ofertas";
            // 
            // enPromociónToolStripMenuItem
            // 
            this.enPromociónToolStripMenuItem.Name = "enPromociónToolStripMenuItem";
            this.enPromociónToolStripMenuItem.Size = new System.Drawing.Size(114, 24);
            this.enPromociónToolStripMenuItem.Text = "En promoción";
            // 
            // miCuentaToolStripMenuItem
            // 
            this.miCuentaToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.misDatosToolStripMenuItem,
            this.misPedidosToolStripMenuItem,
            this.artículosGuardadosToolStripMenuItem,
            this.mensajesToolStripMenuItem,
            this.cerrarSesiónToolStripMenuItem});
            this.miCuentaToolStripMenuItem.Name = "miCuentaToolStripMenuItem";
            this.miCuentaToolStripMenuItem.Size = new System.Drawing.Size(86, 24);
            this.miCuentaToolStripMenuItem.Text = "Mi cuenta";
            // 
            // misDatosToolStripMenuItem
            // 
            this.misDatosToolStripMenuItem.Name = "misDatosToolStripMenuItem";
            this.misDatosToolStripMenuItem.Size = new System.Drawing.Size(217, 26);
            this.misDatosToolStripMenuItem.Text = "Mis datos";
            // 
            // misPedidosToolStripMenuItem
            // 
            this.misPedidosToolStripMenuItem.Name = "misPedidosToolStripMenuItem";
            this.misPedidosToolStripMenuItem.Size = new System.Drawing.Size(217, 26);
            this.misPedidosToolStripMenuItem.Text = "Mis pedidos";
            // 
            // artículosGuardadosToolStripMenuItem
            // 
            this.artículosGuardadosToolStripMenuItem.Name = "artículosGuardadosToolStripMenuItem";
            this.artículosGuardadosToolStripMenuItem.Size = new System.Drawing.Size(217, 26);
            this.artículosGuardadosToolStripMenuItem.Text = "Artículos guardados";
            // 
            // mensajesToolStripMenuItem
            // 
            this.mensajesToolStripMenuItem.Name = "mensajesToolStripMenuItem";
            this.mensajesToolStripMenuItem.Size = new System.Drawing.Size(217, 26);
            this.mensajesToolStripMenuItem.Text = "Mensajes";
            // 
            // cerrarSesiónToolStripMenuItem
            // 
            this.cerrarSesiónToolStripMenuItem.Name = "cerrarSesiónToolStripMenuItem";
            this.cerrarSesiónToolStripMenuItem.Size = new System.Drawing.Size(217, 26);
            this.cerrarSesiónToolStripMenuItem.Text = "Cerrar sesión";
            // 
            // miCarritoToolStripMenuItem
            // 
            this.miCarritoToolStripMenuItem.Name = "miCarritoToolStripMenuItem";
            this.miCarritoToolStripMenuItem.Size = new System.Drawing.Size(85, 24);
            this.miCarritoToolStripMenuItem.Text = "Mi carrito";
            // 
            // ayudaToolStripMenuItem
            // 
            this.ayudaToolStripMenuItem.Name = "ayudaToolStripMenuItem";
            this.ayudaToolStripMenuItem.Size = new System.Drawing.Size(63, 24);
            this.ayudaToolStripMenuItem.Text = "Ayuda";
            // 
            // linkLabel1
            // 
            this.linkLabel1.AutoSize = true;
            this.linkLabel1.Location = new System.Drawing.Point(170, 382);
            this.linkLabel1.Name = "linkLabel1";
            this.linkLabel1.Size = new System.Drawing.Size(148, 17);
            this.linkLabel1.TabIndex = 9;
            this.linkLabel1.TabStop = true;
            this.linkLabel1.Text = "Tarjeta gráfica NVIDIA";
            // 
            // linkLabel2
            // 
            this.linkLabel2.AutoSize = true;
            this.linkLabel2.Location = new System.Drawing.Point(680, 382);
            this.linkLabel2.Name = "linkLabel2";
            this.linkLabel2.Size = new System.Drawing.Size(130, 17);
            this.linkLabel2.TabIndex = 10;
            this.linkLabel2.TabStop = true;
            this.linkLabel2.Text = "Procesadores AMD";
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox3.Image")));
            this.pictureBox3.Location = new System.Drawing.Point(661, 185);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(216, 173);
            this.pictureBox3.TabIndex = 12;
            this.pictureBox3.TabStop = false;
            this.pictureBox3.Click += new System.EventHandler(this.pictureBox3_Click);
            // 
            // directorySearcher1
            // 
            this.directorySearcher1.ClientTimeout = System.TimeSpan.Parse("-00:00:01");
            this.directorySearcher1.ServerPageTimeLimit = System.TimeSpan.Parse("-00:00:01");
            this.directorySearcher1.ServerTimeLimit = System.TimeSpan.Parse("-00:00:01");
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(146, 185);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(227, 181);
            this.pictureBox2.TabIndex = 15;
            this.pictureBox2.TabStop = false;
            // 
            // numericUpDown1
            // 
            this.numericUpDown1.Location = new System.Drawing.Point(705, 131);
            this.numericUpDown1.Name = "numericUpDown1";
            this.numericUpDown1.Size = new System.Drawing.Size(120, 22);
            this.numericUpDown1.TabIndex = 16;
            this.numericUpDown1.ValueChanged += new System.EventHandler(this.numericUpDown1_ValueChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(586, 132);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(113, 17);
            this.label1.TabIndex = 17;
            this.label1.Text = "Mostrar artículos";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(25, 131);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(256, 22);
            this.dateTimePicker1.TabIndex = 18;
            this.dateTimePicker1.ValueChanged += new System.EventHandler(this.dateTimePicker1_ValueChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(395, 260);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(236, 17);
            this.label2.TabIndex = 19;
            this.label2.Text = "Tiempo restante en oferta  23:59:59";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(851, 109);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(86, 17);
            this.label3.TabIndex = 20;
            this.label3.Text = "Ordenar por";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.ClientSize = new System.Drawing.Size(1032, 450);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.dateTimePicker1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.numericUpDown1);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.linkLabel2);
            this.Controls.Add(this.linkLabel1);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.textBox3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.vScrollBar1);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "BuildAPC";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.NotifyIcon notifyIcon1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.VScrollBar vScrollBar1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem inicioToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem categoríasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ordenadoresToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sobremesaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ultrabooksToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem chromebooksToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem portátilesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem gamingToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem paraTrabajarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem periféricosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ratonesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tecladoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem altavocesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tarjetasGráficasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem procesadoresToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ofertasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem enPromociónToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem miCuentaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem misDatosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem misPedidosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem artículosGuardadosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mensajesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cerrarSesiónToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem miCarritoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ayudaToolStripMenuItem;
        private System.Windows.Forms.LinkLabel linkLabel1;
        private System.Windows.Forms.LinkLabel linkLabel2;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.DirectoryServices.DirectorySearcher directorySearcher1;
        private System.Windows.Forms.HelpProvider helpProvider1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.NumericUpDown numericUpDown1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
    }
}

