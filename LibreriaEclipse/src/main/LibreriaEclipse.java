package main;

import java.util.Scanner;


public class LibreriaEclipse {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner input = new Scanner(System.in);
		int opcion = input.nextInt();
		System.out.println("Introduce una opcion:"
				+ "1- Comprobar entero"
				+ "2- Rellenar array"
				+ "3- Contar hasta el 100"
				+ "4- Sumar arrays"
				+ "5- Salir");
		switch(opcion) {
		case 1:
			System.out.println("Introduce cadena");
			String cadena = input.nextLine();
			
			if(Libreria.esEntero(cadena)) {
				System.out.println("Es entero");
			}else{
				System.out.println("No es entero");
			};
			break;
		case 2:
			
			Libreria.rellenar(input.nextLine());
			break;
		case 3:
			System.out.println("Introduce un entero");
			int entero = input.nextInt();
			Libreria.contar(entero);
			break;
			
		case 4:	
			int[] array1 = new int[5];
			int[] array2 = new int[5];
			for(int i=0;i<array1.length;i++) {
				System.out.println("Rellena el primer array con enteros");
				array1[i] = input.nextInt();
				
			}
			for(int i=0;i<array2.length;i++) {
				System.out.println("Rellena el segundo array con enteros");
				array2[i] = input.nextInt();
				
			}
			Libreria.sumarArrays(array1,array2);
			
			break;
		case 5:
			break;
			
		default:
			System.out.println("Opcion no valida");
			break;
		}
		
			
		input.close();	
	}

}
